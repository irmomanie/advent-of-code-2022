module Aoc.Day3

open System
open Input
open FSharpPlus

let splitInHalf characterLists =
    let left, right = characterLists |> List.splitAt (characterLists.Length / 2)
    left :: right :: []
    
let findDuplicate (input: list<char list>) =
    (input |> List.reduce (fun x y -> x |> List.filter (fun c -> y |> List.contains c)))
    |> List.distinct |> List.exactlyOne
   
let inline charToInt (c:char) =
    match c with
    | _ when Char.IsLower c -> int c - 96
    | _ -> int c - 38
    
let runDay03 input =
    printfn $"Running day three with input: %s{input}"
    let lines = readLines input |> Seq.toList
    printfn $"lines: %A{lines}"
    
    let characterLists =
        lines |> List.map (fun line -> line.ToCharArray() |> List.ofArray)
    
    let split = characterLists |> List.map splitInHalf
    printfn $"split: %A{split}"
    
    let duplicates = split |> List.map findDuplicate
    printfn $"duplicates: %A{duplicates}"
    
    let score = duplicates |> List.sumBy charToInt
    printfn $"score: %A{score}"
    
    // part 2
    let groups = characterLists |> List.chunkBySize 3 
    printfn $"groups: %A{groups}"
    
    let groupDuplicates = groups |> List.map findDuplicate
    printfn $"groups duplicates: %A{groupDuplicates}"
    
    let groupScore = groupDuplicates |> List.sumBy charToInt
    printfn $"group score: %A{groupScore}"
