module Aoc.Day4

open System
open FSharpPlus.Data
open Input
open FSharpPlus 

let parseRange (input: string) =
    let i = input.Split("-") |> Array.map int
    (i[0], i[1])

let splitLineToPair (line: string) =
    let s = line.Split(',')
    (parseRange s[0], parseRange s[1])
    
    
let contains (x: int * int, y: int * int) =
    match x, y with
    | (a, b), (c, d) when a <= c && b >= d -> true
    | (a, b), (c, d) when a >= c && b <= d -> true
    | _ -> false

let overlaps (x: int * int, y: int * int) =
    match x, y with
    | (a, b), (c, d) when b >= c && a < d-> true
    | (a, b), (c, d) when d >= a && c < b-> true
    | _ -> false

let runDay04 input =
    printfn $"Running day four with input: %s{input}"
    let lines = readLines input |> Seq.toList
    printfn $"lines: %A{lines}"

    let pairs = lines |> List.map splitLineToPair
    printfn $"pairs: %A{pairs}"
    
    let contains = pairs |> List.map contains
    printfn $"contains: %A{contains}"
    
    let totalContains = contains |> List.filter id
    printfn $"total contains: %A{totalContains.Length}"

    // part 2    
    let overlaps = pairs |> List.map overlaps
    printfn $"overlaps: %A{overlaps}"

    let totalOverlaps = overlaps |> List.filter id
    printfn $"total overlaps: %A{totalOverlaps.Length}"
