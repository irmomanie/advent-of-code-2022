module Aoc.Day5

open System
open FSharpPlus.Data
open Input
open FSharpPlus 

let parseStacks (stacks: string list) =
    stacks |> List.filter (fun e -> e.Trim().Length > 0)
    |> List.map (fun e -> e.ToCharArray() |> List.ofArray )
    |> List.transpose
    |> List.filter (fun e -> e |> List.rev |> List.head |> Char.IsDigit)
    |> List.map (fun e -> (e |> List.filter (fun c -> not <| Char.IsWhiteSpace(c))))

let parseSingleMove (move: string list) =
    match move with
    | "move" :: n :: "from" :: f :: "to" :: [t] -> (int n, int f, int t)
    | _ -> failwith "Unable to parse move"
    
let parseMoves (moves: string list) =
    moves |> List.map (fun e -> e.Split(' ') |> List.ofArray)
    |> List.map parseSingleMove
    
let moveStack(move: int * int * int, stacks: char list list)(moverVersion: char list -> char list) =
    let (n, f, t) = move
    let part, remain = stacks[f-1] |> List.splitAt n
    let updated = (part |> moverVersion) @ stacks[t-1] 
    stacks |> List.updateAt( f-1)(remain) |> List.updateAt(t-1)(updated)
    
let rec executeMoves (moves: (int * int * int) list, stacks: char list list)(moverVersion: char list -> char list) =
    match moves with
    | nextMove :: tail ->
        executeMoves(tail, moveStack(nextMove, stacks)(moverVersion))(moverVersion)
    | [] -> stacks
    
let extractMessage (stacks: char list list) =
    stacks |> List.map(fun e -> e |> List.head) |> Array.ofList |> String

let runDay05 input =
    printfn $"Running day five with input: %s{input}"
    let lines = readLines input |> Seq.toList
    printfn $"lines: %A{lines}"
    
    let moveInput, stackInput = lines |> List.partition (fun e -> e.StartsWith "move")
    printfn $"moves input: %A{moveInput}"
    printfn $"stack input: %A{stackInput}"
    
    let stacks = stackInput |> parseStacks
    printfn $"stacks: %A{stacks}"
    
    let moves = moveInput |> parseMoves
    printfn $"moves: %A{moves}"
    
    let finalStacks9000 = executeMoves(moves, stacks)(List.rev)
    printfn $"final stacks: %A{finalStacks9000}"
    
    let message = finalStacks9000 |> extractMessage
    printfn $"message: %A{message}"

    // part 2
    let finalStacks9001 = executeMoves(moves, stacks)(id)
    printfn $"final stacks 9001: %A{finalStacks9000}"
    
    let message = finalStacks9001 |> extractMessage
    printfn $"message 9001: %A{message}"
    
