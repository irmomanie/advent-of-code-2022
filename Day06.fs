module Aoc.Day6

open System
open FSharpPlus.Data
open Input
open FSharpPlus 

let rec findMarker (window: int)(input: string) =
    let windows = input.ToCharArray() |> Array.windowed window
    let index = windows |> Array.findIndex (fun e -> Array.distinct(e).Length = window)
    index + window    
    
let runDay06 input =
    printfn $"Running day five with input: %s{input}"
    let lines = readLines input |> Seq.toList
    printfn $"lines: %A{lines}"
    
    let markers = lines |> List.map (findMarker 4)
    printfn $"markers: %A{markers}"

    // part 2
    let markersTwo = lines |> List.map (findMarker 14)
    printfn $"markers 2: %A{markersTwo}"
    
    
