module Aoc.Day2

open Input
open FSharpPlus

type Choice =
    | Rock
    | Paper
    | Scissors

type Result =
    | Win
    | Lose
    | Draw
    
let parseWantedResult char =
    match char with
    | 'X' -> Lose
    | 'Y' -> Draw
    | 'Z' -> Win
    | _ -> failwith $"Unable to parse character {char} to Result"
    
let parseChoice char =
    match char with
    | 'A' | 'X' -> Rock
    | 'B' | 'Y' -> Paper
    | 'C' | 'Z' -> Scissors
    | _ -> failwith $"Unable to parse character {char} to Choice"
    
let choiceValue choice =
    match choice with
    | Rock -> 1
    | Paper -> 2
    | Scissors -> 3
    
let wantedChoice (opponent, result) =
    match (opponent, result) with
    | x, Draw -> x
    | Rock, Win -> Paper
    | Rock, Lose -> Scissors
    | Paper, Win -> Scissors
    | Paper, Lose -> Rock
    | Scissors, Win -> Rock
    | Scissors, Lose -> Paper
    
let computeResult (opponent, you) =
    match (opponent, you) with
    | Rock, Paper -> Win
    | Paper, Scissors -> Win
    | Scissors, Rock -> Win
    | _ when opponent = you -> Draw
    | _ -> Lose
    
let resultValue result =
    match result with
    | Win -> 6
    | Lose -> 0
    | Draw -> 3
    
let parseGameElements theirElement yourElement (element: string) =
    let chars = element.ToCharArray() |> Array.toList
    match chars with
    | head :: ' ' :: [ tail ] -> (theirElement head, yourElement tail)
    | _ -> failwith $"Unable to parse input {chars}"
    
let computeScore elements =
    let scores = elements |> List.map computeResult |> List.map resultValue
    let choiceValues = elements |> List.map (fun (_, y) -> choiceValue y)
    (scores @ choiceValues) |> List.sum

let runDay02 input =
    printfn $"Running day two with input: %s{input}"
    let lines = readLines input |> Seq.toList
    printfn $"lines: %A{lines}"
    
    let elements = lines |> List.map (parseGameElements parseChoice parseChoice)
    printfn $"Elements: %A{elements}"
    
    let score = elements |> computeScore
    printfn $"Score: %A{score}"
    
    // Part 2
    let wanted = lines |> List.map (parseGameElements parseChoice parseWantedResult)
    printfn $"Wanted elements: %A{wanted}"
    
    let finalElements = wanted |> List.map (fun (o, y) -> (o, wantedChoice (o, y)))
    printfn $"Final elements: %A{finalElements}"
    
    let finalScore = finalElements |> computeScore
    printfn $"Final score: %A{finalScore}"
    
