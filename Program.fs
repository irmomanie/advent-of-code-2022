﻿open Aoc
open Day1
open Day2
open Day3
open Day4
open Day5
open Day6

let projectRoot = __SOURCE_DIRECTORY__

// runDay01  (projectRoot + "/inputs/day01.input.test.txt")
// runDay01  (projectRoot + "/inputs/day01.input.txt")

// runDay02  (projectRoot + "/inputs/day02.input.test.txt")
// runDay02  (projectRoot + "/inputs/day02.input.txt")

// runDay03  (projectRoot + "/inputs/day03.input.test.txt")
// runDay03  (projectRoot + "/inputs/day03.input.txt")

// runDay04  (projectRoot + "/inputs/day04.input.test.txt")
// runDay04  (projectRoot + "/inputs/day04.input.txt")

// runDay05  (projectRoot + "/inputs/day05.input.test.txt")
// runDay05  (projectRoot + "/inputs/day05.input.txt")

runDay06  (projectRoot + "/inputs/day06.input.test.txt")
runDay06  (projectRoot + "/inputs/day06.input.txt")
