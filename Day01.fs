module Aoc.Day1

open Input
open FSharpPlus

let runDay01 input =
    printfn $"Running day one with input: %s{input}"
    let lines = readLines input
    
    let splitNumbers = lines |> Seq.split [[""]] |> Seq.map (Seq.map int)
    let summed = splitNumbers |> Seq.map Seq.sum
    let summedSorted = summed |> Seq.sortDescending
    let high = summedSorted |> Seq.head
    printfn $"Highest: %i{high}"
    
    let top3sum = summedSorted |> Seq.take 3 |> Seq. sum 
    printfn $"Sum top 3: %i{top3sum}"
